#!/bin/bash

final_value=0

format () {
  val=$(LC_CTYPE=C printf '%d' "'$1")
  if [[ $1 == [A-Z] ]];
  then
    val=$(($val-38))
  elif [[ $1 == [a-z] ]];
  then
    val=$(($val-96))
  fi
  echo $val
}

tables=()

while IFS= read -r rucksack 
do
  tables=(${tables[@]} "$rucksack")
  if [[ ${#tables[@]} -ne 3 ]]; then
    continue
  else
    same=''

    echo ${tables[@]}
    for c in $(echo ${tables[0]} | grep -o .)
    do
      if [[ "${tables[1]}" = *"${c}"* ]] && [[ "${tables[2]}" = *"${c}"* ]]
      then
        same=${c}
      fi
    done

    echo $same
    new_value=$(format $same)
    final_value=$(($final_value + $new_value))
    tables=()
  fi

done < rucksack.txt

echo $final_value

