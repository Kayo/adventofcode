#!/bin/bash

final_value=0

format () {
  val=$(LC_CTYPE=C printf '%d' "'$1")
  if [[ $1 == [A-Z] ]];
  then
    val=$(($val-38))
  elif [[ $1 == [a-z] ]];
  then
    val=$(($val-96))
  fi
  echo $val
}

while IFS= read -r rucksack 
do
  first_half=${rucksack:0:${#rucksack}/2}
  second_half=${rucksack:${#rucksack}/2} 
  same=''
  for c in $(echo $first_half | grep -o .)
  do
    if [[ "${second_half}" = *"${c}"* ]] 
    then
      same=${c} 
    fi
  done

  new_value=$(format $same)
  final_value=$(($final_value + $new_value))
done < rucksack.txt

echo $final_value

