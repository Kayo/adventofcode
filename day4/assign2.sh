#!/bin/bash

count=0

while IFS= read -r assignments 
do
  # On split avec la virgule
  IFS="," read -ra tuple <<< "$assignments"
  # Decouper pour avoir les 4 valeurs
  IFS="-" read -ra first <<< "${tuple[0]}"
  IFS="-" read -ra second <<< "${tuple[1]}"

  # Comparer les deux valeurs à chaque fois 
  #echo $assignments
  #echo "[[ ${first[0]} -le ${second[1]} ]] && [[ ${first[1]} -ge ${second[0]} ]] && [[ ${first[0]} -ge ${second[0]} ]] && [[ ${first[1]} -le ${second[1]} ]] || [[ ${second[0]} -le ${first[1]} ]] && [[ ${second[1]} -ge ${first[0]} ]] && [[ ${second[1]} -le ${first[1]} ]] && [[ ${second[0]} -ge ${first[0]} ]]"
  if [[ ${first[0]} -le ${second[1]} && ${first[0]} -ge ${second[0]} ]] || [[ ${first[1]} -le ${second[1]} && ${first[1]} -ge ${second[0]} ]] || [[ ${second[0]} -le ${first[1]} && ${second[0]} -ge ${first[0]} ]] || [[ ${second[1]} -le ${first[1]} && ${second[1]} -ge ${first[0]} ]]; then
    echo "YES !"
    echo ${first[@]}
    echo ${second[@]}
    count=$((count+1))
  fi

done < assignments.txt

echo $count
