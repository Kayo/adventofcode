import sys

def did_i_won(opponent_play, my_play):
  if opponent_play == 'A': # Pierre
    if my_play == 'X': # Pierre
      return 3
    elif my_play == 'Y': # Papier
      return 6
    elif my_play == 'Z': # Ciseau 
      return 0
  elif opponent_play == 'B': # Papier 
    if my_play == 'X': # Pierre
      return 0
    elif my_play == 'Y': # Papier
      return 3
    elif my_play == 'Z': # Ciseau 
      return 6
  elif opponent_play == 'C': # Ciseau
    if my_play == 'X': # Pierre
      return 6
    elif my_play == 'Y': # Papier
      return 0
    elif my_play == 'Z': # Ciseau 
      return 3

# win_or_lose : X if lose, Z if win, Y if equality
def what_i_want(opponent_play, win_or_lose):
  if opponent_play == 'A': # Pierre
    if win_or_lose == 'X': # Perdre - Ciseau 
      return 3
    elif win_or_lose == 'Y': # Equality - Pierre 
      return 1
    elif win_or_lose == 'Z': # Gagner - Papier 
      return 2
  elif opponent_play == 'B': # Papier 
    if win_or_lose == 'X': # Perdre - Pierre
      return 1
    elif win_or_lose == 'Y': # Equality - Papier 
      return 2
    elif win_or_lose == 'Z': # Gagner - Ciseau 
      return 3
  elif opponent_play == 'C': # Ciseau
    if win_or_lose == 'X': # Perdre - Papier 
      return 2
    elif win_or_lose == 'Y': # Equality - Ciseau 
      return 3
    elif win_or_lose == 'Z': # Gagner - Pierre 
      return 1

with open('pierrefeuilleciseau.txt', 'r') as f:
  rounds = f.read().splitlines()    
  score = 0
  for round in rounds:
    opponent_play = round.split()[0]
    my_play = round.split()[1]
    score += what_i_want(opponent_play, my_play)
    if my_play == 'X': # Lose 
      score += 0
    elif my_play == 'Y': # Equality 
      score += 3
    elif my_play == 'Z': # Win 
      score += 6
  print(score)


